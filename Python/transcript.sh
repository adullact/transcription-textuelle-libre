#!/usr/bin/env bash

set -e

NAME="$1"
NAME_WITHOUT_EXT="${NAME%.*}"
ffmpeg -i "${NAME_WITHOUT_EXT}.webm" -acodec pcm_s16le -ar 16000 -ac 1 "${NAME_WITHOUT_EXT}.wav"
./transcript.py "${NAME_WITHOUT_EXT}.wav" 2>/dev/null >"${NAME_WITHOUT_EXT}.txt"