# Transcription textuelle libre

Comment réaliser la transcription textuelle d'une vidéo ou d'une bande son en utilisant des logiciels libres

## Pré-requis

* Linux Mint 20.2 ou Ubuntu 20.04
* Python 3
* ffmpeg 4.2.4
* [vosk-api](https://github.com/alphacep/vosk-api) 0.0.32
* le [modèle français pour Vosk](https://alphacephei.com/vosk/models) (`vosk-model-fr-0.6-linto-2.2.0`, poids 1,5Go)
* faire un lien symbolique `model` depuis le dossier de travail vers le dossier portant le modèle fran_ais décompressé

## Génération de la transcription

La génération se fait en plusieurs étapes : 

1. Convertir la bande son au format PCM Mono WAV
2. Générer la scription

### Conversion de la bande son au format PCM Mono

Soit `seminaire.webm` l'enregistrement video dont nous souhaitons produire la transcription.

```shell
NAME="seminaire"
ffmpeg -i "${NAME}.webm" -acodec pcm_s16le -ar 16000 -ac 1 "${NAME}.wav"
```

La commande ci-dessus est valable quel que soit le format de la video (webm, ogg, mp4...).
L'extension du fichier de sorti **doit** être `.wav`.

### Génération de la transcription

```shell
cd Python
NAME="seminaire"
./transcript.py "${NAME}.wav" 2>/dev/null >"${NAME}.txt"
```

Et pour avoir un visuel au fur et à mesure (dans un autre terminal) : `tail -f seminaire.txt`

### Temps pour générer la transcription

Il faut compter un rapport de 1/4 ou 1/5 par rapport à la durée de la vidéo / bande son.

Exemple : pour une vidéo de 2h15, il faut environ 25 minutes pour générer la transcription.
